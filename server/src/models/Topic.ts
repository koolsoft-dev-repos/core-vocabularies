import { Schema, Types } from "mongoose";
import Topic from "../../../common/models/Topic";
import { instance } from "../utils/mongoose";

const topicSchema = new Schema({
  courseId: {
    type: Types.ObjectId,
    ref: 'Course'
  },
  parentId: {
    type: Types.ObjectId,
    ref: 'Topic'
  },
  topicExerciseId: {
    type: Types.ObjectId,
    ref: 'TopicExercise'
  },
  name: String,
  type: Number,
  status: Number,
  childType: Number,
  orderIndex: Number,
}, { versionKey: false });

export const TopicModel = instance.model<Topic>('Topic', topicSchema);
