import { Schema } from "mongoose";
import TopicExercise from "../../../common/models/TopicExercise";
import { instance } from "../utils/mongoose";

const topicExerciseSchema = new Schema({
  questionsNum: Number,
  contentType: Number
}, { versionKey: false });

export const TopicExerciseModel = instance.model<TopicExercise>('TopicExercise', topicExerciseSchema);
