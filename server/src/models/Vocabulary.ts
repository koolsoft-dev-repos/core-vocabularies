import { Schema } from "mongoose";
import Vocabulary from "../../../common/models/Vocabulary";
import { mainConnection } from "../utils/mongoose";

const vocabularySchema = new Schema({
  text: String,
  soundUrl: String
}, { versionKey: false, timestamps: true });

export const VocabularyModel = mainConnection.model<Vocabulary>('Vocabulary', vocabularySchema);
