import { Schema, Types } from "mongoose";
import Card from "../../../common/models/Card";
import { instance } from "../utils/mongoose";

const cardSchema = new Schema({
  parentId: Types.ObjectId,
  question: {
    text: String,
    image: String,
    sound: String,
    hint: String
  },
  answer: {
    texts: [String],
    choices: [String],
    image: String,
    sound: String,
    hint: String
  },
  hasChild: Number,
  type: Number,
  status: Number
}, { versionKey: false });

export const CardModel = instance.model<Card>('Card', cardSchema);
