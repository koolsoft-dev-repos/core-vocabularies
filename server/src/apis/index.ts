import { Router } from "express";
import courseRouters from "../modules/courses/course.routers";
import topicRouters from "../modules/topics/topic.routers";
import vocabRouters from "../modules/vocabularies/vocab.routers";
import cardRouters from "../modules/cards/card.routers";

const router = Router();

router.use(courseRouters);
router.use(topicRouters);
router.use(vocabRouters);
router.use(cardRouters);

export default router;
