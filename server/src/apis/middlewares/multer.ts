import Multer from "multer";

export const multerHandler = Multer({
    storage: Multer.memoryStorage(),
    limits: {
        fileSize: 50 * 1024 * 1024, // no larger than 5mb, you can change as needed.
    },
});