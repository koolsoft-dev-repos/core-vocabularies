import { Storage } from "@google-cloud/storage";
import axios from "axios";
import PQueue from "p-queue";
import Card from "../../../../common/models/Card";
import Vocabulary from "../../../../common/models/Vocabulary";
import { VocabularyModel } from "../../models/Vocabulary";
import dotenv from "../../utils/dotenv";
import { VocabsSocket } from "../../utils/sockets";
import CardService from "../cards/card.services";
import TopicService from "../topics/topic.services";

dotenv.config();

const BUCKET_NAME = process.env.GCS_BUCKET;
const BASE_FORDER = process.env.GCS_BASE_FOLDER;
const PROJECT_ID = process.env.GCS_PROJECT_ID;
const PRIVATE_KEY = (process.env.GCS_PRIVATE_KEY)?.replace(/\\n/g, '\n');
const CLIENT_EMAIL = process.env.GCS_CLIENT_EMAIL;
const CLIENT_ID = process.env.GCS_CLIENT_ID;

if (!BUCKET_NAME || !BASE_FORDER || !PROJECT_ID || !PRIVATE_KEY || !CLIENT_EMAIL || !CLIENT_ID) {
  throw new Error("Invalid GCS Config");
}
const storage = new Storage({
  projectId: PROJECT_ID,
  credentials: {
    type: "service_account",
    private_key: PRIVATE_KEY.replace(/\\n/g, '\n'),
    client_email: CLIENT_EMAIL,
    client_id: CLIENT_ID
  }
});
const bucket = storage.bucket(BUCKET_NAME);

export default class VocabService {
  static getGoogleTTSURL(query: string) {
    return `https://translate.google.com.vn/translate_tts?ie=UTF-8&q=${query}&tl=en-us&client=tw-ob`;
  }

  static async uploadBufferToGCP(args: { buffer: ArrayBuffer; fileName: string, sound?: string }): Promise<string> {
    const { buffer, fileName, sound } = args;
    const remoteFile = bucket.file(`${BASE_FORDER}sounds/${fileName}`);
    // Kiểm tra xem file đã tồn tại chưa
    // const [exists] = await remoteFile.exists();
    // console.log("exists : ", exists);

    // if (exists) {
    //   return sound || `https://storage.googleapis.com/${bucket.name}/${remoteFile.name}`
    // }
    return new Promise((resolve, reject) => {
      const blobStream = remoteFile.createWriteStream({
        metadata: {
          contentType: "audio/mpeg",
          contentDisposition: "attachment",
          metadata: {
            originalBytes: buffer.byteLength
          }
        },
      });

      blobStream
        .on("finish", async () => {
          await remoteFile.makePublic();
          resolve(`https://storage.googleapis.com/${bucket.name}/${remoteFile.name}`);
        })
        .on("error", (error) => {
          // if (error.message)
          // reject(new Error(`Unable to upload image, something went wrong ${error}`));
          resolve(`https://storage.googleapis.com/${bucket.name}/${remoteFile.name}`);
        })
        .end(buffer);
    })
  }

  static async createOneVocab(args: { text: string; cardId?: string; sound?: string }) {
    const { text, cardId, sound } = args;
    if (!text) return { soundUrl: '' };

    console.log("search text : ", text);

    const { data, status } = await axios.get(this.getGoogleTTSURL(encodeURIComponent(text)), { responseType: "arraybuffer" });
    if (status !== 200) return { soundUrl: '' };
    const fileName = `${text.replace(/[^a-zA-Z0-9]/g, "").trim()}.mp3`;
    const soundUrl = await this.uploadBufferToGCP({ buffer: data, fileName, sound });
    console.log("soundUrl : ", soundUrl);
    await VocabularyModel.findOneAndUpdate({ text }, { $set: { soundUrl } }, { upsert: true }).exec();

    if (cardId) {
      const card = await CardService.updateCardById({ _id: cardId, updates: { "question.sound": soundUrl, lastUpdate: Date.now() } });
      return { soundUrl, card };
    }
    return { soundUrl, card: null };
  }

  static async getVocabByText(args: { text: string }) {
    const vocab = await VocabularyModel.findOne({ text: args.text });
    return vocab ? new Vocabulary(vocab) : null;
  }

  static async createVocabsByTopic(args: { topicId: string; clientId?: string; sync?: boolean; forceReplace?: boolean }) {
    const cards = await CardService.getCardsByParent({ parentId: args.topicId });
    await Promise.all(cards.map(async (card) => {
      // const _syncedCard: Card & { syncUrl?: string } = { ...card };
      const vocab = await VocabularyModel.findOne({ text: card.question.text });
      if (!vocab || vocab?.soundUrl !== (card.question?.sound ?? "") || args.forceReplace) {
        const vocabSoundUrl = vocab?.soundUrl;
        if (vocabSoundUrl && !!vocabSoundUrl.match(/(.*)_manual\.mp3$/g)) {
          // _syncedCard.syncUrl = vocabSoundUrl;
          // if (args.sync) {
          //   _syncedCard.question.sound = vocabSoundUrl;
          // }
          console.log("exist vocabSoundUrl text : ", card.question.text);
          
          await CardService.updateCardById({ _id: card._id, updates: { "question.sound": vocabSoundUrl, lastUpdate: Date.now() } });
        } else {
          const { soundUrl, card: newCard } = await this.createOneVocab({ text: card.question?.text || "", cardId: card._id, sound: vocab?.soundUrl });
          // _syncedCard.syncUrl = soundUrl;
          // if (args.sync && newCard) {
          //   _syncedCard.question.sound = newCard.question.sound;
          // }
        }
      }
    }))
    return ""
  }

  static async uploadAndSync(args: { text: string, file: Express.Multer.File, cardId: string }) {
    const { text, file, cardId } = args;
    const fileName = `${text.replace(/\s/g, '-')}_manual.mp3`;
    const { buffer } = file;
    const soundUrl = await this.uploadBufferToGCP({ fileName, buffer });
    VocabularyModel.findOneAndUpdate({ text: text }, { $set: { soundUrl } }, { upsert: true }).exec();
    if (cardId) {
      CardService.updateCardById({ _id: cardId, updates: { "question.sound": soundUrl } });
    }
    return soundUrl;
  }
}