import { Router } from "express";
import asyncHandler from "../../utils/asyncHandler";
import CourseService from "./course.services";

const router = Router();

router.get('/categories', asyncHandler(async (req, res) => {
  const parentId: string | null = (req.query.parentId as string) ?? null;
  const limitCourses: number | undefined = req.query.limitCourses ? Number(req.query.limitCourses) : undefined;

  const data = await CourseService.getCategories({ parentId, limitCourses });
  return res.json(data);
}));

export default router;
