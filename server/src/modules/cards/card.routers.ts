import { Router } from "express";
import asyncHandler from "../../utils/asyncHandler";
import CardService, { CardFields } from "./card.services";

const router = Router();

router.patch("/cards/:cardId", asyncHandler(async (req, res) => {
  const _id = req.params.cardId;
  const updates = <{ [key in CardFields]?: any }>req.body;
  const data = await CardService.updateCardById({ _id, updates });
  return res.json(data);
}));

router.post("/sync-cards-sound", asyncHandler(async (req, res) => {
  const { cardIds } = <{ cardIds: string[] }>req.body;
  const data = await CardService.syncCardsSound({ cardIds });
  return res.json(data);
}))

export default router;