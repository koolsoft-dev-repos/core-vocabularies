import cors from "cors";
import express from "express";
import helmet from "helmet";
import { createServer } from "http";
import path from "path";
import router from "./apis";
import { errorHandler, notFoundHandler } from "./apis/middlewares/errorHandlers";
import dotenv from "./utils/dotenv";
import { initSocket } from "./utils/sockets";

const app = express();
const PORT = process.env.PORT || 8081;
const PUBLIC_URL = process.env.PUBLIC_URL;

dotenv.config();

app.use(helmet());
app.use(cors({
  origin: (process.env.NODE_ENV || "development") === "production" ? (process.env.ALLOWED_ORIGIN ? process.env.ALLOWED_ORIGIN.split(',') : true) : true,
  allowedHeaders: "X-PINGOTHER, Content-Type, Authorization, X-Forwarded-For, x-requested-with",
  methods: "GET, POST, PUT, PATCH, DELETE, HEAD, OPTIONS",
  optionsSuccessStatus: 200
}));
app.use(express.json({ limit: "50mb" }));
app.use(express.urlencoded({ extended: true, limit: "50mb" }));

if (PUBLIC_URL) {
  const _publicUrl = PUBLIC_URL.startsWith('/') ? PUBLIC_URL : `/${PUBLIC_URL}`;
  app.use(`${PUBLIC_URL}`, express.static(path.resolve(__dirname, '..', 'public')));
  app.get(`${PUBLIC_URL}/*`, (req, res) => {
    res.sendFile(path.resolve(__dirname, '..', 'public', 'index.html'));
  });
}

app.use('/api-v', router);
app.use(errorHandler);
app.use(notFoundHandler);

const httpServer = createServer(app);
initSocket(httpServer, (io) => {
  if (!!io) {
    console.log('[INFO] Initiated Socket Server');
  }
});

httpServer.listen(PORT, () => {
  console.log('[INFO] Server is running on port: ', PORT);
});


