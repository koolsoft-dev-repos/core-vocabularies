import { useEffect, useState } from "react";
import socketIO, { Socket } from "socket.io-client";

const useSocket = (args: {
  enabled?: boolean;
  url?: string;
  roomId?: string;
  onConnected?: () => void;
}) => {
  const { enabled, url, roomId, onConnected } = args;
  const [socket, setSocket] = useState<Socket | null>(null);

  useEffect(() => {
    if (!enabled || !url) return;

    const socket = socketIO(`${url}`, {
      path: '/api-v/socket-io/',
      transports: ["websocket"]
    });

    socket.emit("join", roomId);
    socket.on("connect", () => {
      console.log("CONNECTED SOCKET");
      if (onConnected) onConnected();
    });

    socket.on("disconnect", () => {
      console.log("DISCONNECTED SOCKET");
    });

    socket.on('reconnect', () => {
      console.log('RECONNECTED SOCKET');
      socket.emit('join', roomId);
    });

    setSocket(socket);

    return () => {
      socket.disconnect();
    }

  }, [enabled]);

  return {
    socket,
    leave: () => {
      if (socket) {
        socket.emit("leave", roomId);
      }
    }
  }
}

export default useSocket;
