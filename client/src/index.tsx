import { ThemeProvider } from "@mui/material/styles";
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider as ReduxProvider } from "react-redux";
import App from "./App";
import store from './store';
import './styles/global.scss';
import theme from "./styles/theme";
import { SnackbarProvider } from "notistack";
import { PersistGate } from 'redux-persist/integration/react';
import { persistStore } from 'redux-persist';

const persitor = persistStore(store);

ReactDOM.render(
  <ReduxProvider store={store}>
    <PersistGate loading={null} persistor={persitor}>
      <ThemeProvider theme={theme}>
        <SnackbarProvider maxSnack={10} anchorOrigin={{ vertical: "top", horizontal: "right" }} autoHideDuration={3000}>
          <App />
        </SnackbarProvider>
      </ThemeProvider>
    </PersistGate>
  </ReduxProvider>,
  document.getElementById("root")
);