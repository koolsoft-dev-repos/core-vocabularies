export const convertHTMLToText = (text: string) => {
    if (text.includes("<")) {
        let tempDiv = document.createElement('div');
        tempDiv.innerHTML = text;

        // Tìm phần tử cha bất kỳ (có thể là div, p, hay bất kỳ phần tử nào)
        let parentElement = tempDiv.firstElementChild;
        if (!parentElement) return;
        // Loại bỏ các thẻ <style> và comment bên trong phần tử cha
        let childNodes = parentElement.childNodes;
        for (let i = childNodes.length - 1; i >= 0; i--) {
            let node: any = childNodes[i];
            if (node.nodeType === Node.ELEMENT_NODE && node.tagName === 'STYLE') {
                // Nếu là thẻ <style>, xóa nó
                parentElement.removeChild(node);
            } else if (node.nodeType === Node.COMMENT_NODE) {
                // Nếu là comment, xóa nó
                parentElement.removeChild(node);
            }
        }

        // Lấy nội dung text trực tiếp từ phần tử cha, không lấy từ thẻ con
        let textContent = '';
        parentElement.childNodes.forEach(node => {
            if (node.nodeType === Node.TEXT_NODE) {
                textContent += node.nodeValue;
            }
        });
        // Loại bỏ khoảng trắng dư thừa
        return textContent.trim();
    }
    return null
}