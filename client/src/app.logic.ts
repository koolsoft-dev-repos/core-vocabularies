import Category from "@@/../common/models/Category";
import Topic from "@@/../common/models/Topic";

enum ActionTypes {
  SET_ROOT_CATEGORIES,
  SET_CHILD_CATEGORIES,
  SET_CURRENT_ROOT_CATEGORY_ID,
  SET_CURRENT_CHILD_CATEGORY_ID,
  SET_CURRENT_COURSE_ID,
  SET_CURRENT_CATEGORY,
  SET_LIST_TOPICS,
  SET_MAP_SYNCING
}

interface AppAction {
  type: ActionTypes;
  categories?: Category[];
  category?: Category;
  categoryId?: string;

  courseId?: string;
  parentTopicId?: string;
  topics?: Topic[];
  syncingTopicId?: string;
  syncing?: boolean;
}

type AppState = {
  rootCategories: Category[];
  childCategories: Category[];
  currentRootCategoryId: string;
  currentChildCategoryId: string;
  currentCourseId: string;
  currentCategory: Category | null;
  mapTopic: {
    [parentId: string]: Topic[];
  },
  mapSyncing: {
    [syncingTopicId: string]: boolean;
  }
}

export const appInitState: AppState = {
  rootCategories: [],
  childCategories: [],
  currentRootCategoryId: '--root--',
  currentChildCategoryId: '--root--',
  currentCourseId: '--root--',
  currentCategory: null,
  mapTopic: {
    ['main']: []
  },
  mapSyncing: {}
}

export const appReducer = (state: AppState, action: AppAction): AppState => {
  switch (action.type) {
    case ActionTypes.SET_ROOT_CATEGORIES:
      return {
        ...state,
        rootCategories: action.categories!
      }

    case ActionTypes.SET_CHILD_CATEGORIES:
      return {
        ...state,
        childCategories: action.categories!
      }

    case ActionTypes.SET_CURRENT_ROOT_CATEGORY_ID:
      return {
        ...state,
        currentRootCategoryId: action.categoryId!,
        currentCategory: state.rootCategories.find((e) => e._id === action.categoryId) ?? null
      }

    case ActionTypes.SET_CURRENT_CHILD_CATEGORY_ID:
      return {
        ...state,
        currentChildCategoryId: action.categoryId!,
        currentCategory: state.childCategories.find((e) => e._id === action.categoryId) ?? null
      }

    case ActionTypes.SET_CURRENT_COURSE_ID:
      return {
        ...state,
        currentCourseId: action.courseId!
      }


    case ActionTypes.SET_CURRENT_CATEGORY:
      return {
        ...state,
        currentCategory: action.category!
      }

    case ActionTypes.SET_LIST_TOPICS:
      return {
        ...state,
        mapTopic: {
          ...state.mapTopic,
          [`${action.parentTopicId}`]: action.topics!
        }
      }

    case ActionTypes.SET_MAP_SYNCING:
      return {
        ...state,
        mapSyncing: {
          ...state.mapSyncing, [`${action.syncingTopicId}`]: !!action.syncing
        }
      }
        
    default:
      return state;
  }
}

export const setCategoriesAction = (categories: Category[], root?: boolean): AppAction => ({
  type: root ? ActionTypes.SET_ROOT_CATEGORIES : ActionTypes.SET_CHILD_CATEGORIES, categories
});

export const setCurrentRootCategoryIdAction = (categoryId: string): AppAction => ({
  type: ActionTypes.SET_CURRENT_ROOT_CATEGORY_ID, categoryId
});

export const setCurrentChildCategoryIdAction = (categoryId: string): AppAction => ({
  type: ActionTypes.SET_CURRENT_CHILD_CATEGORY_ID, categoryId
});

export const setCurrentCourseIdAction = (courseId: string): AppAction => ({
  type: ActionTypes.SET_CURRENT_COURSE_ID, courseId
});

export const setCurrentCategoryAction = (category: Category): AppAction => ({
  type: ActionTypes.SET_CURRENT_CATEGORY, category
});

export const setListTopicsAction = (parentTopicId: string, topics: Topic[]): AppAction => ({
  type: ActionTypes.SET_LIST_TOPICS, parentTopicId, topics
});

export const setMapSyncingAction = (syncingTopicId: string, syncing: boolean): AppAction => ({
  type: ActionTypes.SET_MAP_SYNCING, syncingTopicId, syncing
});

