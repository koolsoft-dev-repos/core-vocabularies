import { combineReducers, configureStore } from "@reduxjs/toolkit";
import topicReducer from "./redux/topic.slice";
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const currentTopicPersistConfig = {
  key: 'current_topic',
  storage,
  migrate: (state: any) => {
    state.topicReducer.cards = [];
    state.topicReducer.topicLoading = true;
    return Promise.resolve(state)
  }
}

const reducers = combineReducers({
  topicReducer: topicReducer
})

const store = configureStore({
  reducer: persistReducer(currentTopicPersistConfig, reducers)
});


export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export default store;