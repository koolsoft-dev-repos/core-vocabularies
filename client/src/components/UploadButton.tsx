import { apiUploadFileVocab } from '@/apis/vocab.apis';
import { Button } from '@mui/material';
import { makeStyles } from "@mui/styles";
import React, { ChangeEvent } from 'react'

const useStyles = makeStyles((_) => ({
    uploadInput: {
        display: "none"
    },
    buttonUpload: {
        textTransform: "none"
    }
}));

const UploadButton = (props: {
    id?: string;
    label?: string;
    text: string;
    cardId: string;
    onUploadFinished?: (args: string) => void;
    onUploadSuccess?: () => void;
    onUploadError?: () => void;
}) => {
    const classes = useStyles();
    const id = `upload-button${Math.random()}`;
    const { label, text, cardId, onUploadFinished, onUploadSuccess, onUploadError } = props;

    const handleChangeFile = async (e: ChangeEvent<HTMLInputElement>) => {
        const file = e.target.files ? e.target.files[0] : null;
        if (file) {
            if (file.size > 33554432) {
                if (typeof onUploadError !== 'undefined') {
                    onUploadError();
                }
            }
            // const formData = new FormData();
            // formData.append('file', file);
            try {
                apiUploadFileVocab({ text, file, cardId })
                    .then(({soundUrl}) => {
                        if (typeof onUploadSuccess !== 'undefined') {
                            onUploadSuccess();
                        }
                        if (typeof onUploadFinished !== 'undefined') {
                            onUploadFinished(soundUrl);
                        }
                    });
            } catch (error) {
                if (typeof onUploadError !== 'undefined') {
                    onUploadError();
                }
            }
        }
    }

    return (
        <div>
            <input
                className={classes.uploadInput}
                id={id}
                multiple
                type="file"
                onChange={handleChangeFile}
            />
            <label htmlFor={id}>
                <Button className={classes.buttonUpload} variant="outlined" size="small" component="span">
                    {label || "Tải file"}
                </Button>
            </label>
        </div>
    )
}

export default UploadButton
