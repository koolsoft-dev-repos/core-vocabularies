export const STATUS_DELETED = -1;
export const STATUS_PRIVATE = 0;
export const STATUS_PUBLIC = 1;
export const STATUS_OPEN = 4;

export const TOPIC_TYPE_LESSON = 1;
export const TOPIC_TYPE_EXERCISE = 2;
export const TOPIC_TYPE_TEST = 3;

export const TOPIC_CONTENT_TYPE_CARD = 0;
export const TOPIC_CONTENT_TYPE_FILE_PDF = 1;
export const EXAM_TYPE_IELTS = 2;
export const EXAM_TYPE_TOEIC = 3;
export const EXAM_TYPE_TOEFL = 4;
export const EXAM_TYPE_SAT = 5;
export const TOPIC_CONTENT_TYPE_FLASH_CARD = 6;
