export default class TopicExercise {
  _id: any;
  questionsNum: number;
  contentType: number;

  constructor(args: any = {}) {
    this._id = args._id;
    this.questionsNum = args.questionsNum;
    this.contentType = args.contentType;
  }
}