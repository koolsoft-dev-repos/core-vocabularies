import TopicExercise from "./TopicExercise";
import { isObject } from "../utils";

export default class Topic {
  _id: any;
  courseId: string;
  parentId: string | null;
  name: string;
  type: number;
  status: number;
  childType: number;
  orderIndex: number;
  topicExercise?: TopicExercise;

  constructor(args: any = {}) {
    this._id = args._id;
    this.courseId = args.courseId;
    this.parentId = args.parentId;
    this.name = args.name;
    this.type = args.type;
    this.status = args.status;
    this.childType = args.childType;
    this.orderIndex = args.orderIndex;
    if (!!args.topicExercise || isObject(args.topicExerciseId)) {
      this.topicExercise = new TopicExercise(args.topicExercise || args.topicExerciseId)
    }
  }
}