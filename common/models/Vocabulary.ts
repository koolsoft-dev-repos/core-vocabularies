export default class Vocabulary {
  _id: any;
  text: string;
  soundUrl: string;
  createdAt: Date;
  updatedAt: Date;

  constructor(args: any = {}) {
    this._id = args._id;
    this.text = args.text;
    this.soundUrl = args.soundUrl;
    this.createdAt = args.createdAt;
    this.updatedAt = args.updatedAt;
  }
}